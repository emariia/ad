public class Cycles {

	public boolean dfs(int v, boolean[] visited, int[][] graph) {

		for (int i = 0; i < graph[v].length; i++) {
			if (graph[v][i] == 1) {
				if (visited[i] && i != v) {// schon besucht aber kein parent
					return false; // Kreis entdeckt
				}
				if (!visited[i]) {
					visited[i] = true;
					dfs(i, visited, graph);
				}
			}
		}
		return true;
	}

	// bestimmt ob der ungerichtete Graph kreisfrei ist
	public boolean NotGerichtetKreisfrei(int[][] graph) {

		boolean[] visited = new boolean[graph.length];
		for (int i = 0; i < graph.length; i++) {
			if (!visited[i]) {
				visited[i] = true;
				return dfs(i, visited, graph);
			}
		}
		return true;
	}

	public boolean NotGerichtetKreisfreiUnionFind(int[][] graph) {

		int parent[] = new int[graph.length]; // array fuer union find
		for (int i = 0; i < parent.length; i++) {
			parent[i] = i; // am Anfang jeder parent von sich selbst
		}
		for (int i = 0; i < graph.length; i++) {
			for (int j = 0; j < graph.length; j++) {
				if (find(parent, i) == find(parent, j)) {
					return false;
				} else {
					union(parent, i, j);
				}
			}
		}
		return true;
	}

	public int find(int[] parent, int v) {
		if (parent[v] == v) {
			return v;
		} else {
			return find(parent, parent[v]);
		}
	}

	public void union(int[] parent, int u, int v) {
		int u_parent = find(parent, u);
		int v_parent = find(parent, v);
		if (u_parent > v_parent) {
			parent[v_parent] = u_parent;
		} else {
			parent[u_parent] = v_parent;
		}
	}

	// finds cycles in directed graphs
	public void hasCycle(int[][] graph) {
		boolean[] been = new boolean[graph.length]; // schon einmal besucht
		boolean[] done = new boolean[graph.length]; // keine unbesuchten Nachbaren mehr
		int[] parent = new int[graph.length];// Vorgaenger
		int[] detected = new int[2];
		detected[0] = -1;// default
		detected[1] = -1;// default
		for (int i = 0; i < parent.length; i++) {
			parent[i] = -1; // am Anfang "null"
		}
		for (int i = 0; i < graph.length; i++) {
			if (detected[0] != -1) {//wenn ein Kreis bereits erkannt wurde
				break;
			}
			if (!done[i]) {//wenn noch nicht komplett bearbeitet
				dfsModif(i, been, done, parent, detected, graph);
			}
		}
		if (detected[0] == -1) {
			System.out.println("Es gibt keinen Kreis");
		} else {
			int v = detected[0];
			while (v != detected[1]) {
				System.out.print(" "+ v );// die Knoten des Kreises ausgeben 
				v = parent[v];
			}
		}

	}

//
//modifiziertes dfs
//bestimmt ob die Knoten komplett abgearbeitet wurden
//speichert den vorgaenger
	public void dfsModif(int v, boolean[] been, boolean[] done, int[] parent, int[] detected, int[][] graph) {

		for (int i = 0; i < graph[v].length; i++) {
			if (graph[v][i] == 1) {
				if (!been[i]) {
					been[i] = true;
					parent[i] = v;
					dfsModif(i, been, done, parent, detected, graph);

				} else {
					detected[0] = v;
					detected[1] = i;
				}
			}
			if (i == graph.length - 1) { // wenn alle potenzielle Nachbarn kontrolliert wurden
				done[v] = true;
				been[v] = false;
			}
		}
	}
}
